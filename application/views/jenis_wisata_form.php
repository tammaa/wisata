<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        
    </head>
    <body>
        <h2 style="margin-top:0px">Jenis_wisata <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Nama Jeniswisata <?php echo form_error('nama_jeniswisata') ?></label>
            <input type="text" class="form-control" name="nama_jeniswisata" id="nama_jeniswisata" placeholder="Nama Jeniswisata" value="<?php echo $nama_jeniswisata; ?>" />
        </div>
	    <input type="hidden" name="id_jeniswisata" value="<?php echo $id_jeniswisata; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('jenis_wisata') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>