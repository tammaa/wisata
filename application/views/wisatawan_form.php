<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        
    </head>
    <body>
        <h2 style="margin-top:0px">Wisatawan <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Nama Wisatawan <?php echo form_error('nama_wisatawan') ?></label>
            <input type="text" class="form-control" name="nama_wisatawan" id="nama_wisatawan" placeholder="Nama Wisatawan" value="<?php echo $nama_wisatawan; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Jenis Kelamin <?php echo form_error('jenis_kelamin') ?></label>
            <input type="text" class="form-control" name="jenis_kelamin" id="jenis_kelamin" placeholder="Jenis Kelamin" value="<?php echo $jenis_kelamin; ?>" />
        </div>
	    <div class="form-group">
            <label for="alamat">Alamat <?php echo form_error('alamat') ?></label>
            <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Alamat"><?php echo $alamat; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">No Telp <?php echo form_error('no_telp') ?></label>
            <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="No Telp" value="<?php echo $no_telp; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Hobi <?php echo form_error('hobi') ?></label>
            <input type="text" class="form-control" name="hobi" id="hobi" placeholder="Hobi" value="<?php echo $hobi; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Usia <?php echo form_error('usia') ?></label>
            <input type="text" class="form-control" name="usia" id="usia" placeholder="Usia" value="<?php echo $usia; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Created <?php echo form_error('created') ?></label>
            <input type="text" class="form-control" name="created" id="created" placeholder="Created" value="<?php echo $created; ?>" />
        </div>
	    <input type="hidden" name="id_wisatawan" value="<?php echo $id_wisatawan; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('wisatawan') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>