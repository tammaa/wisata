<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Kabupaten <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="kabupaten">Kabupaten <?php echo form_error('kabupaten') ?></label>
            <textarea class="form-control" rows="3" name="kabupaten" id="kabupaten" placeholder="Kabupaten"><?php echo $kabupaten; ?></textarea>
        </div>
	    <input type="hidden" name="id_kabupaten" value="<?php echo $id_kabupaten; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('kabupaten') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>