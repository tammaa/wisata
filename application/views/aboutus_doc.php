<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Aboutus List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Kategori</th>
		<th>Deskripsi</th>
		<th>Gambar</th>
		<th>Status</th>
		<th>Created</th>
		<th>Edited</th>
		
            </tr><?php
            foreach ($aboutus_data as $aboutus)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $aboutus->kategori ?></td>
		      <td><?php echo $aboutus->deskripsi ?></td>
		      <td><?php echo $aboutus->gambar ?></td>
		      <td><?php echo $aboutus->status ?></td>
		      <td><?php echo $aboutus->created ?></td>
		      <td><?php echo $aboutus->edited ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>