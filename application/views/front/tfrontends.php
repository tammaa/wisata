<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Rekomendasi Wisata</title>
		
        <meta name="description" content="description">
		
		<!-- Mobile Specific Meta
		================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/front/img/favicon.png" />
		
		<!-- CSS
		================================================== -->
		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/font-awesome.min.css">
		<!-- bootstrap.min css -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/bootstrap.min.css">
		<!-- Animate.css -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/animate.css">
		<!-- Owl Carousel -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/owl.carousel.css">		
		<!-- Media Queries -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/responsive.css">
        <!-- Grid Component css -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/component.css">
        <!-- Slit Slider css -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/slit-slider.css">
        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/main.css">
        <!-- Media Queries -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/media-queries.css">


		<!--
		Google Font
		=========================== -->                    
		
		<!-- Titillium Web -->
		<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,200' rel='stylesheet' type='text/css'>
		<!-- Source Sans Pro -->
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300' rel='stylesheet' type='text/css'>
		<!-- Oswald -->
		<link href='http://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
		<!-- Raleway -->
		<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
		
		<!-- Modernizer Script for old Browsers -->
        <script src="<?php echo base_url();?>assets/front/js/modernizr-2.6.2.min.js"></script>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
          ga('create', 'UA-54152927-1', 'auto');
          ga('send', 'pageview');
        
        </script>
        <style>
			.dropdown {
			    position: relative;
			    display: inline-block;
			}

			.dropdown-content {
			    display: none;
			    position: absolute;
			    background-color: #333;
			    min-width: 160px;
			    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			    padding: 12px 16px;
			}

			.dropdown:hover .dropdown-content {
			    display: block;
			}
		</style>

    </head>
	
    <body class="blog-page">
	    <!--
	    Start Preloader
	    ==================================== -->
		<!-- <div id="loading-mask">
			<div class="loading-img">
				<img alt="Meghna Preloader" src="<?php echo base_url();?>assets/front/img/preloader.gif"  />
			</div>
		</div> -->
        <!--
        End Preloader
        ==================================== -->
        
        <!-- 
        Fixed Navigation
        ==================================== -->
        <header class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
						<h1 id="logo">
							<img src="" alt="Rekomendasi Tempat Wisata" />
						</h1>
					</a>
                </div>

                <nav class="collapse navbar-collapse navbar-right" >
                    <ul id="nav" class="nav navbar-nav">
                        <li><a href="<?php echo base_url();?>wstw/index">Home</a></li>
                        <li><a href="<?php echo base_url();?>wstw/registrasi">Registrasi</a></li>
                        	<div class="dropdown">
                        		<span><br><li><a href="<?php echo base_url();?>log">Welcome, <?php echo $name;?></a></li></span>
                        		<div class="dropdown-content">
								  <p><a href="<?php echo base_url();?>log/logout">Logout</a></p>
								</div>
							</div>
                    </ul>

                </nav><!-- /.navbar-collapse -->
            </div>
        </header>
        <!--
        End Fixed Navigation
        ==================================== -->
        
        
        <!-- Start Blog Banner
        ==================================== -->
        <section id="blog-banner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                       
                        <div class="blog-icon">
                            <i class="fa fa-book fa-4x"></i>
                        </div>
                        <div class="blog-title">
                            <h1>Rekomendasi Tempat Wisata</h1>
                        </div>
                        
					</div>     <!-- End col-lg-12 -->
				</div>	    <!-- End row -->
			</div>       <!-- End container -->
		</section>    <!-- End Section -->
        
        
        <!-- Start Blog Post Section
        ==================================== -->
        
        <!-- page dinamis -->
        <?php $this->load->view($page); ?>

		<!-- Start Footer Section
		========================================== -->
		<footer id="footer" class="bg-one">
			<div class="container">
			    <div class="row wow fadeInUp" data-wow-duration="500ms">
					<div class="col-lg-12">
						
						<!-- Footer Social Links -->
						<div class="social-icon">
							<ul>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								<li><a href="#"><i class="fa fa-youtube"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
							</ul>
						</div>
						<!--/. End Footer Social Links -->

						<!-- copyright -->
						<div class="copyright text-center">
							<h1>Tugas Akhir 2016</h1> <br />
							<p>Copyright &copy; 2016. All Rights Reserved.</p>
						</div>
						<!-- /copyright -->
						
					</div> <!-- end col lg 12 -->
				</div> <!-- end row -->
			</div> <!-- end container -->
		</footer> <!-- end footer -->
		
		<!-- Back to Top
		============================== -->
		<a href="#" id="scrollUp"><i class="fa fa-angle-up fa-2x"></i></a>
		
		<!-- end Footer Area
		========================================== -->
		
		<!-- 
		Essential Scripts
		=====================================-->
		
		<!-- Main jQuery -->
		<script src="<?php echo base_url();?>assets/front/js/jquery-1.11.0.min.js"></script>
		<!-- Bootstrap 3.1 -->
		<script src="<?php echo base_url();?>assets/front/js/bootstrap.min.js"></script>
		<!-- Slitslider -->
        <script src="<?php echo base_url();?>assets/front/js/jquery.slitslider.js"></script>
        <script src="<?php echo base_url();?>assets/front/js/jquery.ba-cond.min.js"></script>
        <!-- Parallax -->
        <script src="<?php echo base_url();?>assets/front/js/jquery.parallax-1.1.3.js"></script>
        <!-- Back to Top -->
		<script src="<?php echo base_url();?>assets/front/js/jquery.scrollUp.min.js"></script>
		<script src="<?php echo base_url();?>assets/front/js/classie.js"></script>
		<!-- Owl Carousel -->
		<script src="<?php echo base_url();?>assets/front/js/owl.carousel.min.js"></script>
        <!-- Portfolio Filtering -->
        <script src="<?php echo base_url();?>assets/front/js/jquery.mixitup.min.js"></script>
		<!-- Custom Scrollbar -->
		<script src="<?php echo base_url();?>assets/front/js/jquery.nicescroll.min.js"></script>
        <!-- Jappear js -->
        <script src="<?php echo base_url();?>assets/front/js/jquery.appear.js"></script>
        <!-- Pie Chart -->
        <script src="<?php echo base_url();?>assets/front/js/easyPieChart.js"></script>
        <!-- tweetie.min -->
        <script src="<?php echo base_url();?>assets/front/js/tweetie.min.js"></script>
        <!-- Google Map API -->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <!-- Highlight menu item -->
        <script src="<?php echo base_url();?>assets/front/js/jquery.nav.js"></script>
        <!-- Sticky Nav -->
        <script src="<?php echo base_url();?>assets/front/js/jquery.sticky.js"></script>
        <!-- Number Counter Script -->
        <script src="<?php echo base_url();?>assets/front/js/jquery.countTo.js"></script>
		<!-- jQuery Easing -->
		<script src="<?php echo base_url();?>assets/front/js/jquery.easing-1.3.pack.js"></script>
		<!-- wow.min Script -->
		<script src="<?php echo base_url();?>assets/front/js/wow.min.js"></script>
		<!-- For video responsive -->
		<script src="<?php echo base_url();?>assets/front/js/jquery.fitvids.js"></script>
		<!-- Grid js -->
        <script src="<?php echo base_url();?>assets/front/js/grid.js"></script>
        <!-- Custom js -->
		<script src="<?php echo base_url();?>assets/front/js/custom.js"></script>
    </body>
</html>