<section id="contact-us">
			<div class="container">
				<div class="row">
					<!-- Contact Details -->
					<div class="contact-info col-md-6 wow fadeInUp" data-wow-duration="500ms">
						<h3>Registrasi Pengguna</h3>
						<p>Pengguna dipersilahkan melakukan Registrasi untuk dapat menggunakan fasilitas Rekomendasi Tempat Wisata. Mulai dari Tambah rekomendasian tempat wisata, dan Tambah Testimoni tempat wisata dan memberikan rating terhadap Tempat Wisata.</p>
						<div class="contact-details">
							<div class="con-info clearfix">
								<i class="fa fa-home fa-lg"></i>
								<span>Jalan Dipatiukur, Bandung, Indonesia</span>
							</div>
							
							<div class="con-info clearfix">
								<i class="fa fa-phone fa-lg"></i>
								<span>Phone: +022-31-000-000</span>
							</div>
							
							<div class="con-info clearfix">
								<i class="fa fa-fax fa-lg"></i>
								<span>Fax: +022-31-000-000</span>
							</div>
							
							<div class="con-info clearfix">
								<i class="fa fa-envelope fa-lg"></i>
								<span>Email: rekomendasiwisata@gmail.com</span>
							</div>
						</div>
					</div>
					<!-- / End Contact Details -->
						
					<!-- Contact Form -->
					<div class="contact-form col-md-6 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
						<form id="contact-form" method="post" action="sendmail.php" role="form">
						
							<div class="form-group">
								<input type="text" placeholder="Your Name" class="form-control" name="name" id="name">
							</div>
							
							<div class="form-group">
								<input type="email" placeholder="Your Email" class="form-control" name="email" id="email">
							</div>
							
							<div class="form-group">
								<input type="password" placeholder="Your Password" class="form-control" name="password" id="password">
							</div>
							<div class="form-group">
								<input type="password" placeholder="Re-type Your Password" class="form-control" name="repassword" id="repassword">
							</div>

							<div class="form-group">
								<textarea rows="6" placeholder="Alamat" class="form-control" name="alamat" id="alamat"></textarea>	
							</div>
							
							<div id="mail-success" class="success">
								Thank you. The Mailman is on His Way :)
							</div>
							
							<div id="mail-fail" class="error">
								Sorry, don't know what happened. Try later :(
							</div>
							
							<div id="cf-submit">
								<input type="submit" id="contact-submit" class="btn btn-transparent" value="Submit">
							</div>						
							
						</form>
					</div>
					<!-- ./End Contact Form -->
				
				</div> <!-- end row -->
			</div> <!-- end container -->
			
			<!-- Google Map -->
			<!-- <div class="google-map wow fadeInDown" data-wow-duration="500ms">
				<div id="map-canvas"></div>
			</div> -->	
			<!-- /Google Map -->
			
		</section> <!-- end section -->