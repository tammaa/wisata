<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        
    </head>
    <body>
        <h2 style="margin-top:0px">Tempat_wisata <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="nama_tempatw">Nama Tempatw <?php echo form_error('nama_tempatw') ?></label>
            <textarea class="form-control" rows="3" name="nama_tempatw" id="nama_tempatw" placeholder="Nama Tempatw"><?php echo $nama_tempatw; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="kabupaten">Kabupaten </label>
            <select class="form-control" name="kabupaten" id="kabupaten">
                <?php foreach($kabupaten as $row){?>
                <option value="<?php echo $row->id_kabupaten;?>"><?php echo $row->kabupaten; ?></option>
                <?php }?>
            </select>
            <!-- <textarea class="form-control" rows="3" name="kabupaten" id="kabupaten" placeholder="Kabupaten"></textarea> <?php echo $kabupaten; ?>-->
        </div>
	    <div class="form-group">
            <label for="alamat">Alamat <?php echo form_error('alamat') ?></label>
            <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Alamat"><?php echo $alamat; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="deskripsi">Deskripsi <?php echo form_error('deskripsi') ?></label>
            <textarea class="form-control" rows="3" name="deskripsi" id="deskripsi" placeholder="Deskripsi"><?php echo $deskripsi; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="created">Created <?php echo form_error('created') ?></label>
            <textarea class="form-control" rows="3" name="created" id="created" placeholder="Created"><?php echo $created; ?></textarea>
        </div>
	    <input type="hidden" name="id_tempatwisata" value="<?php echo $id_tempatwisata; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('tempat_wisata') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>