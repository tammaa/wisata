<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css') ?>"/>
        
    </head>
    <body>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <h2 style="margin-top:0px">Wisatawan List</h2>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php echo anchor(site_url('wisatawan/create'), 'Create', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('wisatawan/excel'), 'Excel', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('wisatawan/word'), 'Word', 'class="btn btn-primary"'); ?>
	    </div>
        </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Nama Wisatawan</th>
		    <th>Jenis Kelamin</th>
		    <th>Alamat</th>
		    <th>No Telp</th>
		    <th>Hobi</th>
		    <th>Usia</th>
		    <th>Created</th>
		    <th>Action</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($wisatawan_data as $wisatawan)
            {
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
		    <td><?php echo $wisatawan->nama_wisatawan ?></td>
		    <td><?php echo $wisatawan->jenis_kelamin ?></td>
		    <td><?php echo $wisatawan->alamat ?></td>
		    <td><?php echo $wisatawan->no_telp ?></td>
		    <td><?php echo $wisatawan->hobi ?></td>
		    <td><?php echo $wisatawan->usia ?></td>
		    <td><?php echo $wisatawan->created ?></td>
		    <td style="text-align:center" width="200px">
			<?php 
			echo anchor(site_url('wisatawan/read/'.$wisatawan->id_wisatawan),'Read'); 
			echo ' | '; 
			echo anchor(site_url('wisatawan/update/'.$wisatawan->id_wisatawan),'Update'); 
			echo ' | '; 
			echo anchor(site_url('wisatawan/delete/'.$wisatawan->id_wisatawan),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
			?>
		    </td>
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });
        </script>
    </body>
</html>