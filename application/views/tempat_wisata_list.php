<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css') ?>"/>
        
    </head>
    <body>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <h2 style="margin-top:0px">Tempat Wisata List</h2>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php echo anchor(site_url('tempat_wisata/create'), 'Create', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('tempat_wisata/excel'), 'Excel', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('tempat_wisata/word'), 'Word', 'class="btn btn-primary"'); ?>
	    </div>
        </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Nama Tempatw</th>
		    <th>Kabupaten</th>
		    <th>Alamat</th>
		    <th>Deskripsi</th>
		    <th>Created</th>
		    <th>Action</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($tempat_wisata_data as $tempat_wisata)
            {
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
		    <td><?php echo $tempat_wisata->nama_tempatw ?></td>
		    <td><?php echo $tempat_wisata->kabupaten ?></td>
		    <td><?php echo $tempat_wisata->alamat ?></td>
		    <td><?php echo $tempat_wisata->deskripsi ?></td>
		    <td><?php echo $tempat_wisata->created ?></td>
		    <td style="text-align:center" width="200px">
			<?php 
			echo anchor(site_url('tempat_wisata/read/'.$tempat_wisata->id_tempatwisata),'Read'); 
			echo ' | '; 
			echo anchor(site_url('tempat_wisata/update/'.$tempat_wisata->id_tempatwisata),'Update'); 
			echo ' | '; 
			echo anchor(site_url('tempat_wisata/delete/'.$tempat_wisata->id_tempatwisata),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
			?>
		    </td>
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });
        </script>
    </body>
</html>