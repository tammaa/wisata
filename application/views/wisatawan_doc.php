<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Wisatawan List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Nama Wisatawan</th>
		<th>Jenis Kelamin</th>
		<th>Alamat</th>
		<th>No Telp</th>
		<th>Hobi</th>
		<th>Usia</th>
		<th>Created</th>
		
            </tr><?php
            foreach ($wisatawan_data as $wisatawan)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $wisatawan->nama_wisatawan ?></td>
		      <td><?php echo $wisatawan->jenis_kelamin ?></td>
		      <td><?php echo $wisatawan->alamat ?></td>
		      <td><?php echo $wisatawan->no_telp ?></td>
		      <td><?php echo $wisatawan->hobi ?></td>
		      <td><?php echo $wisatawan->usia ?></td>
		      <td><?php echo $wisatawan->created ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>