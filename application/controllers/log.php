<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class log extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('loginakun_model');
    }

    public function index()
    {
            $log = array(
                'username'  => $this->input->post('username'),
                'password'  => $this->input->post('password'),
                'logged_in' => TRUE
            );

            $this->session->set_userdata($log);
            $status = $this->loginakun_model->getstatus($_SESSION['username']);
            
            if($status == NULL){
                $this->load->view('login_page');
            }else{
                redirect(base_url('wstwn'));    
            }
			
    }

    function logout(){
        $array_items = array('username', 'password', 'logged_in');
        $this->session->unset_userdata($array_items);
        redirect(base_url('log'));
    }
}