<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aboutus extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Aboutus_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'aboutus/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'aboutus/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'aboutus/index.html';
            $config['first_url'] = base_url() . 'aboutus/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Aboutus_model->total_rows($q);
        $aboutus = $this->Aboutus_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'aboutus_data' => $aboutus,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('aboutus_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Aboutus_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_aboutus' => $row->id_aboutus,
		'kategori' => $row->kategori,
		'deskripsi' => $row->deskripsi,
		'gambar' => $row->gambar,
		'status' => $row->status,
		'created' => $row->created,
		'edited' => $row->edited,
	    );
            $this->load->view('aboutus_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('aboutus'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('aboutus/create_action'),
	    'id_aboutus' => set_value('id_aboutus'),
	    'kategori' => set_value('kategori'),
	    'deskripsi' => set_value('deskripsi'),
	    'gambar' => set_value('gambar'),
	    'status' => set_value('status'),
	    'created' => set_value('created'),
	    'edited' => set_value('edited'),
	);
        $this->load->view('aboutus_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'kategori' => $this->input->post('kategori',TRUE),
		'deskripsi' => $this->input->post('deskripsi',TRUE),
		'gambar' => $this->input->post('gambar',TRUE),
		'status' => $this->input->post('status',TRUE),
		'created' => $this->input->post('created',TRUE),
		'edited' => $this->input->post('edited',TRUE),
	    );

            $this->Aboutus_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('aboutus'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Aboutus_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('aboutus/update_action'),
		'id_aboutus' => set_value('id_aboutus', $row->id_aboutus),
		'kategori' => set_value('kategori', $row->kategori),
		'deskripsi' => set_value('deskripsi', $row->deskripsi),
		'gambar' => set_value('gambar', $row->gambar),
		'status' => set_value('status', $row->status),
		'created' => set_value('created', $row->created),
		'edited' => set_value('edited', $row->edited),
	    );
            $this->load->view('aboutus_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('aboutus'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_aboutus', TRUE));
        } else {
            $data = array(
		'kategori' => $this->input->post('kategori',TRUE),
		'deskripsi' => $this->input->post('deskripsi',TRUE),
		'gambar' => $this->input->post('gambar',TRUE),
		'status' => $this->input->post('status',TRUE),
		'created' => $this->input->post('created',TRUE),
		'edited' => $this->input->post('edited',TRUE),
	    );

            $this->Aboutus_model->update($this->input->post('id_aboutus', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('aboutus'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Aboutus_model->get_by_id($id);

        if ($row) {
            $this->Aboutus_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('aboutus'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('aboutus'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('kategori', 'kategori', 'trim|required');
	$this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');
	$this->form_validation->set_rules('gambar', 'gambar', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
	$this->form_validation->set_rules('created', 'created', 'trim|required');
	$this->form_validation->set_rules('edited', 'edited', 'trim|required');

	$this->form_validation->set_rules('id_aboutus', 'id_aboutus', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "aboutus.xls";
        $judul = "aboutus";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Kategori");
	xlsWriteLabel($tablehead, $kolomhead++, "Deskripsi");
	xlsWriteLabel($tablehead, $kolomhead++, "Gambar");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");
	xlsWriteLabel($tablehead, $kolomhead++, "Created");
	xlsWriteLabel($tablehead, $kolomhead++, "Edited");

	foreach ($this->Aboutus_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kategori);
	    xlsWriteLabel($tablebody, $kolombody++, $data->deskripsi);
	    xlsWriteLabel($tablebody, $kolombody++, $data->gambar);
	    xlsWriteNumber($tablebody, $kolombody++, $data->status);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created);
	    xlsWriteLabel($tablebody, $kolombody++, $data->edited);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=aboutus.doc");

        $data = array(
            'aboutus_data' => $this->Aboutus_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('aboutus_doc',$data);
    }

}

/* End of file Aboutus.php */
/* Location: ./application/controllers/Aboutus.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-04-01 05:49:23 */
/* http://harviacode.com */