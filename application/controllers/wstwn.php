<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wstwn extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Wstw_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['name'] = $_SESSION['username'];
        $data['page'] = 'front/wstwpict';
        $this->load->view('front/tfrontends' , $data);
    }

    public function registrasi()
    {
        $data['page'] = 'front/registrasi';
        $this->load->view('front/tfrontends' , $data);
    }

    public function detailwstw()
    {
        $data['page'] = 'front/blog';
        $this->load->view('front/tfrontends' , $data);
    }
    
}