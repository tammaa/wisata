<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tempat_wisata extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tempat_wisata_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $tempat_wisata = $this->Tempat_wisata_model->get_all_wkabupaten();
        // print_r($tempat_wisata);die();

        $data = array(
            'tempat_wisata_data' => $tempat_wisata
        );

        $data['page'] = 'tempat_wisata_list';
        $this->load->view('include/template', $data);
    }

    public function read($id) 
    {
        $row = $this->Tempat_wisata_model->get_by_id_kabupaten($id);
        if ($row) {
            $data = array(
		'id_tempatwisata' => $row->id_tempatwisata,
		'nama_tempatw' => $row->nama_tempatw,
		'kabupaten' => $row->kabupaten,
		'alamat' => $row->alamat,
		'deskripsi' => $row->deskripsi,
		'created' => $row->created,
	    );
            $data['page'] = 'tempat_wisata_read';
        $this->load->view('include/template', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tempat_wisata'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tempat_wisata/create_action'),
	    'id_tempatwisata' => set_value('id_tempatwisata'),
	    'nama_tempatw' => set_value('nama_tempatw'),
	    'kabupaten' => set_value('kabupaten'),
	    'alamat' => set_value('alamat'),
	    'deskripsi' => set_value('deskripsi'),
	    'created' => set_value('created'),
	);
        $data['kabupaten'] = $this->Tempat_wisata_model->get_kabupaten();
        $data['page'] = 'tempat_wisata_form';
        $this->load->view('include/template', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_tempatw' => $this->input->post('nama_tempatw',TRUE),
		'kabupaten' => $this->input->post('kabupaten',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'deskripsi' => $this->input->post('deskripsi',TRUE),
		'created' => $this->input->post('created',TRUE),
	    );

            $this->Tempat_wisata_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tempat_wisata'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tempat_wisata_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tempat_wisata/update_action'),
		'id_tempatwisata' => set_value('id_tempatwisata', $row->id_tempatwisata),
		'nama_tempatw' => set_value('nama_tempatw', $row->nama_tempatw),
		'kabupaten' => set_value('kabupaten', $row->kabupaten),
		'alamat' => set_value('alamat', $row->alamat),
		'deskripsi' => set_value('deskripsi', $row->deskripsi),
		'created' => set_value('created', $row->created),
	    );
            $data['kabupaten'] = $this->Tempat_wisata_model->get_kabupaten();
            $data['page'] = 'tempat_wisata_form';
            $this->load->view('include/template', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tempat_wisata'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_tempatwisata', TRUE));
        } else {
            $data = array(
		'nama_tempatw' => $this->input->post('nama_tempatw',TRUE),
		'kabupaten' => $this->input->post('kabupaten',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'deskripsi' => $this->input->post('deskripsi',TRUE),
		'created' => $this->input->post('created',TRUE),
	    );

            $this->Tempat_wisata_model->update($this->input->post('id_tempatwisata', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tempat_wisata'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tempat_wisata_model->get_by_id($id);

        if ($row) {
            $this->Tempat_wisata_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tempat_wisata'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tempat_wisata'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_tempatw', 'nama tempatw', 'trim|required');
	$this->form_validation->set_rules('kabupaten', 'kabupaten', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');
	$this->form_validation->set_rules('created', 'created', 'trim|required');

	$this->form_validation->set_rules('id_tempatwisata', 'id_tempatwisata', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tempat_wisata.xls";
        $judul = "tempat_wisata";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Tempatw");
	xlsWriteLabel($tablehead, $kolomhead++, "Kabupaten");
	xlsWriteLabel($tablehead, $kolomhead++, "Alamat");
	xlsWriteLabel($tablehead, $kolomhead++, "Deskripsi");
	xlsWriteLabel($tablehead, $kolomhead++, "Created");

	foreach ($this->Tempat_wisata_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_tempatw);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kabupaten);
	    xlsWriteLabel($tablebody, $kolombody++, $data->alamat);
	    xlsWriteLabel($tablebody, $kolombody++, $data->deskripsi);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=tempat_wisata.doc");

        $data = array(
            'tempat_wisata_data' => $this->Tempat_wisata_model->get_all(),
            'start' => 0
        );
        
        $data['page'] = 'tempat_wisata_doc';
        $this->load->view('include/template', $data);
    }

}

/* End of file Tempat_wisata.php */
/* Location: ./application/controllers/Tempat_wisata.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-04-01 14:43:24 */
/* http://harviacode.com */