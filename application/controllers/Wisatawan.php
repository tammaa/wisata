<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wisatawan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Wisatawan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $wisatawan = $this->Wisatawan_model->get_all();

        $data = array(
            'wisatawan_data' => $wisatawan
        );
        $data['page'] = 'wisatawan_list';
        $this->load->view('include/template', $data);
    }

    public function read($id) 
    {
        $row = $this->Wisatawan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_wisatawan' => $row->id_wisatawan,
		'nama_wisatawan' => $row->nama_wisatawan,
		'jenis_kelamin' => $row->jenis_kelamin,
		'alamat' => $row->alamat,
		'no_telp' => $row->no_telp,
		'hobi' => $row->hobi,
		'usia' => $row->usia,
		'created' => $row->created,
	    );
            $data['page'] = 'wisatawan_read';
            $this->load->view('include/template', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wisatawan'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('wisatawan/create_action'),
	    'id_wisatawan' => set_value('id_wisatawan'),
	    'nama_wisatawan' => set_value('nama_wisatawan'),
	    'jenis_kelamin' => set_value('jenis_kelamin'),
	    'alamat' => set_value('alamat'),
	    'no_telp' => set_value('no_telp'),
	    'hobi' => set_value('hobi'),
	    'usia' => set_value('usia'),
	    'created' => set_value('created'),
	);
        $data['page'] = 'wisatawan_form';
        $this->load->view('include/template', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_wisatawan' => $this->input->post('nama_wisatawan',TRUE),
		'jenis_kelamin' => $this->input->post('jenis_kelamin',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
		'hobi' => $this->input->post('hobi',TRUE),
		'usia' => $this->input->post('usia',TRUE),
		'created' => $this->input->post('created',TRUE),
	    );

            $this->Wisatawan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('wisatawan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Wisatawan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('wisatawan/update_action'),
		'id_wisatawan' => set_value('id_wisatawan', $row->id_wisatawan),
		'nama_wisatawan' => set_value('nama_wisatawan', $row->nama_wisatawan),
		'jenis_kelamin' => set_value('jenis_kelamin', $row->jenis_kelamin),
		'alamat' => set_value('alamat', $row->alamat),
		'no_telp' => set_value('no_telp', $row->no_telp),
		'hobi' => set_value('hobi', $row->hobi),
		'usia' => set_value('usia', $row->usia),
		'created' => set_value('created', $row->created),
	    );
            $data['page'] = 'wisatawan_form';
            $this->load->view('include/template', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wisatawan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_wisatawan', TRUE));
        } else {
            $data = array(
		'nama_wisatawan' => $this->input->post('nama_wisatawan',TRUE),
		'jenis_kelamin' => $this->input->post('jenis_kelamin',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
		'hobi' => $this->input->post('hobi',TRUE),
		'usia' => $this->input->post('usia',TRUE),
		'created' => $this->input->post('created',TRUE),
	    );

            $this->Wisatawan_model->update($this->input->post('id_wisatawan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('wisatawan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Wisatawan_model->get_by_id($id);

        if ($row) {
            $this->Wisatawan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('wisatawan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wisatawan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_wisatawan', 'nama wisatawan', 'trim|required');
	$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('no_telp', 'no telp', 'trim|required');
	$this->form_validation->set_rules('hobi', 'hobi', 'trim|required');
	$this->form_validation->set_rules('usia', 'usia', 'trim|required');
	$this->form_validation->set_rules('created', 'created', 'trim|required');

	$this->form_validation->set_rules('id_wisatawan', 'id_wisatawan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "wisatawan.xls";
        $judul = "wisatawan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Wisatawan");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenis Kelamin");
	xlsWriteLabel($tablehead, $kolomhead++, "Alamat");
	xlsWriteLabel($tablehead, $kolomhead++, "No Telp");
	xlsWriteLabel($tablehead, $kolomhead++, "Hobi");
	xlsWriteLabel($tablehead, $kolomhead++, "Usia");
	xlsWriteLabel($tablehead, $kolomhead++, "Created");

	foreach ($this->Wisatawan_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_wisatawan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->jenis_kelamin);
	    xlsWriteLabel($tablebody, $kolombody++, $data->alamat);
	    xlsWriteLabel($tablebody, $kolombody++, $data->no_telp);
	    xlsWriteLabel($tablebody, $kolombody++, $data->hobi);
	    xlsWriteNumber($tablebody, $kolombody++, $data->usia);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=wisatawan.doc");

        $data = array(
            'wisatawan_data' => $this->Wisatawan_model->get_all(),
            'start' => 0
        );
        
        $data['page'] = 'wisatawan_doc';
        $this->load->view('include/template', $data);
    }

}

/* End of file Wisatawan.php */
/* Location: ./application/controllers/Wisatawan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-04-04 20:27:07 */
/* http://harviacode.com */